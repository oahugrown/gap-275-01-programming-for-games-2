
#include "MemoryGame.h"


void PrintWindowsError()
{
	DWORD errorCode = ::GetLastError();
	wchar_t errorString[1024];

	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, 0, errorString, 1024, NULL);

	OutputDebugString(errorString);
}

MemoryGame& MemoryGame::getInstance()
{
	//static - only one istance of this variable in this function. Will always be the same
	static MemoryGame theGame; 
	return theGame;
}


bool MemoryGame::StartUp()
{
	HINSTANCE hInstance = (HINSTANCE)::GetModuleHandle(NULL);

	//Contains the window class attributes that are registered by the RegisterClass function. @MSDN
	WNDCLASSEX windowClass;	//WNDCLASS - Old school. WNDCLASSEX - Modern API
	windowClass.cbSize = sizeof(windowClass);
	windowClass.style = 0;
	windowClass.lpfnWndProc = &WindowProc;
	windowClass.cbWndExtra = 0;
	windowClass.cbClsExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = 0;
	windowClass.hCursor = 0;
	windowClass.hbrBackground = 0;
	windowClass.lpszMenuName = 0;
	windowClass.lpszClassName = L"Win32IntroApp";
	windowClass.hIconSm = 0;
	//Neat way to zero things out ^^^ 
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	if (::RegisterClassEx(&windowClass) == 0)
	{
		PrintWindowsError();
		return false;
	}
	//Creates an overlapped, pop - up, or child window with an extended window style; 
	//otherwise, this function is identical to the CreateWindow function.For more information about creating a window and for full descriptions of the other parameters of CreateWindowEx, see CreateWindow.
	m_hwnd = ::CreateWindowEx(0, L"Win32IntroApp", L"win32 Intro App",
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, hInstance, NULL);

	if (m_hwnd == NULL)
	{
		PrintWindowsError();
		return false;
	}
	//Sets the specified window's show state. @MSDN 
	::ShowWindow(m_hwnd, SW_SHOW);

	::OutputDebugString(L"Created window!");

	m_isRunning = true;
	//Brush with which we are paining the window
	m_bgBrush = ::CreateSolidBrush(RGB(192, 255, 192));
	m_cardBackBrush = ::CreateHatchBrush(HS_DIAGCROSS, RGB(0, 0, 255));
	m_cardFrontBrush = ::CreateSolidBrush(RGB(192, 255, 255));

	m_cards[0].m_isFacedUp = true;

	return true;
}
void MemoryGame::Update()
{
	MSG msg;
	if (::GetMessage(&msg, m_hwnd, 0, 0))
	{
		::DispatchMessage(&msg);
	}
/*
	RECT rect;
	::GetClientRect(MemoryGame::getInstance().m_hwnd, &rect);
	::InvalidateRect(m_hwnd, &rect, TRUE);*/
}
void MemoryGame::Shutdown()
{
	//Deletes brush - stops memory leak 
	::DeleteObject(m_bgBrush);
	::DeleteObject(m_cardBackBrush);
}


MemoryGame::MemoryGame()
	: m_isRunning(false)
	, m_hwnd(NULL)
	, m_bgBrush(NULL)
	, m_cardBackBrush(NULL)
{

}

MemoryGame::~MemoryGame()
{

}

//Message that we recive form windows 
//Event handler - THE BIG DEAL! 
LRESULT CALLBACK MemoryGame::WindowProc(
	HWND   hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	if (uMsg == WM_CLOSE)
	{
		MemoryGame::getInstance().m_isRunning = false;
		return 0;
	}
	else if (uMsg == WM_PAINT)	//Painting the window
	{
		//Storing paint struct
		MemoryGame::getInstance().PaintWindow();
	}

	return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void MemoryGame::PaintWindow()
{
	PAINTSTRUCT pc;
	// HDC ?????????. Thats where we 
	HDC dc = ::BeginPaint(MemoryGame::getInstance().m_hwnd, &pc);
	//Brush with which we are paining the window
	//HBRUSH bgBrush = ::CreateSolidBrush(RGB(0, 255, 0)); 
	//rectangle
	RECT clientRect;
	//Referencing given rectangle
	::GetClientRect(MemoryGame::getInstance().m_hwnd, &clientRect);
	//Fills the background
	::FillRect(dc, &clientRect, m_bgBrush);
	//Deletes brush - stops memory leak 
	//::DeleteObject(m_bgBrush);
	//Stopping the painting process

	for (int i = 0; i < 6; ++i)
	{
		PaintCard(m_cards[i], i, 0);
	}

	::EndPaint(MemoryGame::getInstance().m_hwnd, &pc);
}

void MemoryGame::PaintCard(const MemoryCard& card, int col, int row)
{
	const int cardWith = 50;
	const int cardHeight = 80;

	RECT cardRect;
	//Positionning
	cardRect.left = 10 + (cardWith + 10) * col;
	cardRect.top = 10 + (cardHeight + 10) * row;
	cardRect.right = cardRect.left + 50;
	cardRect.bottom = cardRect.top + 80;

	HDC dc = ::GetDC(m_hwnd);

	if (card.m_isFacedUp)
	{
		::FillRect(dc, &cardRect, m_cardFrontBrush);

		POINT points[3]; 
		points[0].x = cardRect.left; 
		points[0].y = cardRect.bottom;

		points[1].x = cardRect.right;
		points[1].y = cardRect.bottom;

		points[2].x = cardRect.left + cardWith / 2; 
		points[2].y = cardRect.top;


		::Polygon(dc, points, 3);
	}
	else
	{
		::FillRect(dc, &cardRect, m_cardBackBrush);
	}

}
