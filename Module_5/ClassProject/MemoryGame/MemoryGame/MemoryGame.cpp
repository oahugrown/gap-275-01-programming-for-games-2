
#include "MemoryGame.h"
#include <sstream>

#include "SDL2_mixer\include\SDL_mixer.h"

void PrintWindowsError()
{
	DWORD errorCode = ::GetLastError();
	wchar_t errorString[1024];

	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, 0, errorString, 1024, NULL);

	OutputDebugString(errorString);
}

MemoryGame& MemoryGame::getInstance()
{
	//static - only one istance of this variable in this function. Will always be the same
	static MemoryGame theGame; 
	return theGame;
}


bool MemoryGame::StartUp()
{
	//Initializing the SDL - Using sound 
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		return false; //Something went wrong with sdl init


	if (Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG)  < 0)
	{
		::OutputDebugStringA("FAILED: MixInit()");
	}
	//Bigger buffer for music - Smaller buffer for sound effects
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) < 0) //4096
	{
		::OutputDebugStringA("Failed some other shit");
		return false;
	}

	m_cardFlipFX = Mix_LoadWAV("Audio/Explosion.wav");
	
	if (m_cardFlipFX == nullptr)
	{
		::OutputDebugStringA("Failed some shit");
		return false;
	}

	m_bgMusic = Mix_LoadMUS("Audio/Combat music.ogg");
	if (m_bgMusic == nullptr)
	{
		::OutputDebugStringA("Failed some shit");
		return false;
	}

	Mix_PlayMusic(m_bgMusic, -1);

	//SDL_AudioSpec audioSoec;
	//unsigned int  wavLenght = 0; 
	//unsigned char* wavData = nullptr;

	//if (SDL_LoadWAV("Audio/Explosion.wav", &audioSoec, &wavData, &wavLenght) == nullptr)
	//{
	//	::OutputDebugStringA("FAILED TO LOAD FILE");
	//	return false;
	//}

	//Win32 API
	//HINSTANCE hInstance = (HINSTANCE)::GetModuleHandle(NULL);

	////Contains the window class attributes that are registered by the RegisterClass function. @MSDN
	//WNDCLASSEX windowClass;	//WNDCLASS - Old school. WNDCLASSEX - Modern API
	//windowClass.cbSize = sizeof(windowClass);
	//windowClass.style = 0;
	//windowClass.lpfnWndProc = &WindowProc;
	//windowClass.cbWndExtra = 0;
	//windowClass.cbClsExtra = 0;
	//windowClass.hInstance = hInstance;
	//windowClass.hIcon = 0;
	//windowClass.hCursor = ::LoadCursor(NULL, IDC_CROSS);
	//windowClass.hbrBackground = 0;
	//windowClass.lpszMenuName = 0;
	//windowClass.lpszClassName = L"Win32IntroApp";
	//windowClass.hIconSm = 0;
	//Neat way to zero things out ^^^ 
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	//if (::RegisterClassEx(&windowClass) == 0)
	//{
	//	PrintWindowsError();
	//	return false;
	//}
	//Creates an overlapped, pop - up, or child window with an extended window style; 
	//otherwise, this function is identical to the CreateWindow function.For more information about creating a window and for full descriptions of the other parameters of CreateWindowEx, see CreateWindow.
	
	
	
	//m_hwnd = ::CreateWindowEx(0, L"Win32IntroApp", L"Memory Game",
	//	WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, hInstance, NULL);

	//if (m_hwnd == NULL)
	//{
	//	PrintWindowsError();
	//	return false;
	//}

	//SDL API For Creating the window
	m_window = SDL_CreateWindow("Memory Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		640, 480, SDL_WINDOW_SHOWN);

	if (m_window == nullptr)
	{
		SDL_Log(SDL_GetError());
		return false;
	}

	//WIN32
	//Sets the specified window's show state. @MSDN 
	//::ShowWindow(m_hwnd, SW_SHOW);

	::OutputDebugString(L"Created window!");

	m_isRunning = true;
	//Brush with which we are paining the window
	m_bgBrush = ::CreateSolidBrush(RGB(192, 255, 192));
	m_cardBackBrush = ::CreateHatchBrush(HS_DIAGCROSS, RGB(0, 0, 255));
	m_cardFrontBrush = ::CreateSolidBrush(RGB(192, 255, 255));

	m_cards[0].m_isFacedUp = true;

	const int cardWith = 50;
	const int cardHeight = 80;

	int row = 0;
	for (int col = 0; col < 6; ++col)
	{
		RECT& cardRect = m_cards[col].m_cardRect;
		//Positionning
		cardRect.left = 10 + (cardWith + 10) * col;
		cardRect.top = 10 + (cardHeight + 10) * row;
		cardRect.right = cardRect.left + 50;
		cardRect.bottom = cardRect.top + 80;

	}


	for (int index = 0; index < 4; ++index)
	{
		XINPUT_CAPABILITIES caps;
		DWORD status = XInputGetCapabilities(index, 0, &caps);
		if (status == ERROR_DEVICE_NOT_CONNECTED)
		{
			std::stringstream ss;
			ss << "Gamepad " << index + 1 << " not connected" << std::endl;
			::OutputDebugStringA(ss.str().c_str());
		}
		else
		{
			std::stringstream ss;
			ss << "Gamepad " << index + 1 << " connected!" << std::endl;
			::OutputDebugStringA(ss.str().c_str());

			ss << "Subtype: " << (int)caps.SubType << std::endl;
			::OutputDebugStringA(ss.str().c_str());
		}
	}

	XInputEnable(TRUE);

	return true;
}
void MemoryGame::Update()
{
	MSG msg;
	if (::PeekMessage(&msg, m_hwnd, 0, 0, PM_REMOVE))
	{
		::DispatchMessage(&msg);
	}

	XINPUT_KEYSTROKE keystroke; 
	
	//if (XInputGetKeystroke(0, 0, &keystroke) == ERRO)
	if (XInputGetKeystroke(0, 0, &keystroke) == ERROR_SUCCESS)
	{
		if (keystroke.VirtualKey == VK_PAD_A && keystroke.Flags & XINPUT_KEYSTROKE_KEYDOWN)
		{
			MemoryGame::getInstance().m_cards[0].m_isFacedUp = !MemoryGame::getInstance().m_cards[0].m_isFacedUp;
			/*POINT cursorPosition;
			::GetCursorPos(&cursorPosition);
			::ScreenToClient(m_hwnd, &cursorPosition);*/
			//Redraw the window
			RECT clientRect;
			::GetClientRect(m_hwnd, &clientRect);
			//Redraw
			::InvalidateRect(m_hwnd, &clientRect, FALSE);
		}
	}

	
	/*XINPUT_STATE gamePadState;
	if (XInputGetState(0, &gamePadState) == ERROR_SUCCESS)
	{
		if (gamePadState.Gamepad.wButtons & XINPUT_GAMEPAD_A)
		{
			MemoryGame::getInstance().m_cards[0].m_isFacedUp = !MemoryGame::getInstance().m_cards[0].m_isFacedUp;
			POINT cursorPosition; 
			::GetCursorPos(&cursorPosition);
			::ScreenToClient(m_hwnd, &cursorPosition);

		}
	}*/


	///*Moving the card with cursor*/
	//POINT cursorPosition; 
	//::GetCursorPos(&cursorPosition);
	//::ScreenToClient(m_hwnd, &cursorPosition);

	//int cardWidth = m_cards[0].m_cardRect.right - m_cards[0].m_cardRect.left;
	//int cardHeight = m_cards[0].m_cardRect.bottom - m_cards[0].m_cardRect.top;


	//m_cards[0].m_cardRect.left = cursorPosition.x;
	//m_cards[0].m_cardRect.top = cursorPosition.y;
	//m_cards[0].m_cardRect.right = m_cards[0].m_cardRect.left + cardWidth;
	//m_cards[0].m_cardRect.bottom = m_cards[0].m_cardRect.top + cardHeight;
	///*-------------------------------*/

	////Redraw the window
	//RECT clientRect;
	//::GetClientRect(m_hwnd, &clientRect);
	////Redraw
	//::InvalidateRect(m_hwnd, &clientRect, FALSE);

	//Cool Keyboard Update
	//if (::GetAsyncKeyState('A'))
	//{
	//	MemoryGame::getInstance().m_cards[0].m_isFacedUp = !MemoryGame::getInstance().m_cards[0].m_isFacedUp;
	//	//Redraw the window
	//	RECT clientRect;
	//	::GetClientRect(m_hwnd, &clientRect);
	//	//Redraw
	//	::InvalidateRect(m_hwnd, &clientRect, FALSE);
	//}
/*
	RECT rect;
	::GetClientRect(MemoryGame::getInstance().m_hwnd, &rect);
	::InvalidateRect(m_hwnd, &rect, TRUE);*/
}
void MemoryGame::Shutdown()
{
	//Deletes brush - stops memory leak 
	::DeleteObject(m_bgBrush);
	::DeleteObject(m_cardBackBrush);
	//Sounds
	//Clearing the audio
	Mix_FreeChunk(m_cardFlipFX);
	Mix_FreeMusic(m_bgMusic);
	//closing Mixer
	Mix_CloseAudio();
	Mix_Quit();
	//closing SDL
	SDL_Quit();
}


MemoryGame::MemoryGame()
	: m_isRunning(false)
	, m_hwnd(NULL)
	, m_bgBrush(NULL)
	, m_cardBackBrush(NULL)
	, m_cardFlipFX(NULL)
{

}

MemoryGame::~MemoryGame()
{

}

//Message that we recive form windows 
//Event handler - THE BIG DEAL! 
LRESULT CALLBACK MemoryGame::WindowProc(
	HWND   m_hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	if (uMsg == WM_CLOSE)
	{
		MemoryGame::getInstance().m_isRunning = false;
		return 0;
	}
	else if (uMsg == WM_PAINT)	//Painting the window
	{
		//Storing paint struct
		MemoryGame::getInstance().PaintWindow();
	}
	else if (uMsg == WM_KEYDOWN)
	{

		if (wParam >= 0x31 && wParam <= 0x36) // 1 Key
		{
			int cardIndex = wParam - 0x31;
			MemoryGame::getInstance().m_cards[cardIndex].m_isFacedUp = !MemoryGame::getInstance().m_cards[cardIndex].m_isFacedUp;
		}

		//Redraw the window
		RECT clientRect;
		::GetClientRect(m_hwnd, &clientRect);
		//Redraw
		::InvalidateRect(m_hwnd, &clientRect, FALSE);
	}

	else if (uMsg == WM_LBUTTONDOWN)
	{
		//int mouseX = GET_X_LPARAM(lParam);
		//int mouseY = GET_Y_LPARAM(lParam);

		POINT mousePosition; 
		mousePosition.x = GET_X_LPARAM(lParam);
		mousePosition.y = GET_Y_LPARAM(lParam);

		for (int i = 0; i < 6; ++i)
		{
			MemoryCard& card = MemoryGame::getInstance().m_cards[i];
			const RECT& cardRect = MemoryGame::getInstance().m_cards[i].m_cardRect;

			if (::PtInRect(&cardRect, mousePosition))
			{
				card.m_isFacedUp = !card.m_isFacedUp;
				Mix_PlayChannel(-1, MemoryGame::getInstance().m_cardFlipFX, 0);
			}

			/* ONE WAY - ANOTHER WAY ^^^^
			if (mouseX >= cardRect.left && mouseX <= cardRect.right
				&& mouseY >= cardRect.top && mouseY <= cardRect.bottom)
			{
				MemoryGame::getInstance().m_cards[i].m_isFacedUp = !MemoryGame::getInstance().m_cards[i].m_isFacedUp;

			}
			*/
		}

		//Redraw the window
		RECT clientRect;
		::GetClientRect(m_hwnd, &clientRect);
		//Redraw
		::InvalidateRect(m_hwnd, &clientRect, FALSE);
	}

	return ::DefWindowProc(m_hwnd, uMsg, wParam, lParam);
}

void MemoryGame::PaintWindow()
{
	PAINTSTRUCT pc;
	// HDC ?????????. Thats where we 
	HDC dc = ::BeginPaint(MemoryGame::getInstance().m_hwnd, &pc);
	//Brush with which we are paining the window
	//HBRUSH bgBrush = ::CreateSolidBrush(RGB(0, 255, 0)); 
	//rectangle
	RECT clientRect;
	//Referencing given rectangle
	::GetClientRect(MemoryGame::getInstance().m_hwnd, &clientRect);
	//Fills the background
	::FillRect(dc, &clientRect, m_bgBrush);
	//Deletes brush - stops memory leak 
	//::DeleteObject(m_bgBrush);
	//Stopping the painting process

	for (int i = 0; i < 6; ++i)
	{
		PaintCard(m_cards[i], i, 0);
	}

	::EndPaint(MemoryGame::getInstance().m_hwnd, &pc);
}

void MemoryGame::PaintCard(const MemoryCard& card, int col, int row)
{
	HDC dc = ::GetDC(m_hwnd);

	RECT cardRect = card.m_cardRect;

	if (card.m_isFacedUp)
	{
		::FillRect(dc, &cardRect, m_cardFrontBrush);

		POINT points[3]; 
		points[0].x = cardRect.left; 
		points[0].y = cardRect.bottom;

		points[1].x = cardRect.right;
		points[1].y = cardRect.bottom;

		points[2].x = cardRect.left + (cardRect.right - cardRect.left) / 2; 
		points[2].y = cardRect.top;


		::Polygon(dc, points, 3);
	}
	else
	{
		::FillRect(dc, &cardRect, m_cardBackBrush);
	}

	::ReleaseDC(m_hwnd, dc);
}
