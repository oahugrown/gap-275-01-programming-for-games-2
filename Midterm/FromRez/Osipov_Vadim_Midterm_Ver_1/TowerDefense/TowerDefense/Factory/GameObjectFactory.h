#pragma once

#include "../Utility/Singleton.h"
#include "../Tiles/Tile.h"
#include "../Game_Field/GameField.h"

class InteractibleComp;

class GameObjectFactory : public Singleton<GameObjectFactory>
{
public:
	void CreateGroundTile(char tileChar, GameField* pGrid, int posX, int posY);
	void CreateStructure(InteractibleComp* pObjectBase);
};

