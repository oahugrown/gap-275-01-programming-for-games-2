
#pragma once

#include "Tile.h"
#include "../Renderer/RendererComp.h"
class TileGrass : public Tile 
{
	RendererComp* m_pMyRendererComp;
public:
	TileGrass(int x, int y, char* pTextureLocation, int zOrder);
};