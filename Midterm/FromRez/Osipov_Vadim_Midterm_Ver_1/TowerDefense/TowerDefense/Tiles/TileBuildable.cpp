
#include "TileBuildable.h"

#include "../Renderer/Renderer.h"

TileBuildable::TileBuildable(int x, int y, char* pTextureLocation, int zOrder)
	: Tile()
	, m_pMyRendererComp(new RendererComp(0, pTextureLocation, x, y))
	, m_pMyIntercactibleComp(new InteractibleComp(x, y, m_pMyRendererComp->GetRect()))
{

}
