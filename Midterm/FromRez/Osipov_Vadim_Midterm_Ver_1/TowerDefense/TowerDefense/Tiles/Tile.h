//	Tile.h
//	Name: Vadim Osipov
//	Date: 10/16/15

#pragma once

#include "../SDL2/include/SDL.h"

#include "../Components/Component.h"

//---------------------------------------------------------------------------------------------------------------------
// To Rez: 
//		This class is to be highly refactored.
//		I'm thinking to fully get rid of subclasses and keep it to effects/textures instead of actual subclassing. 
//---------------------------------------------------------------------------------------------------------------------

class Tile
{
protected:
	//Saving position of this tile
	SDL_Rect* m_pMyRect;

public: 
	Tile();
	virtual ~Tile() { };

protected:
	//Setter
	void SetRect(SDL_Rect* rect){ m_pMyRect = rect; }
	//Setter
	SDL_Rect* GetRect() { return m_pMyRect; }
};