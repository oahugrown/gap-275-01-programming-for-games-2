//	TowerStructure.h
//	Name: Vadim Osipov
//	Date: 10/17

#pragma once

#include "Structure.h"

#include "../Renderer/RendererComp.h"
#include "../Components/InteractibleComp.h"

class TowerStructure : public Structure 
{
	RendererComp* m_pMyRendererComp;
	InteractibleComp* m_pMyInteractibleComp;

public:
	TowerStructure(InteractibleComp* pBase, int posX, int posY, char* pTextureLocation);
};