//	Structure.h 
//	Name: Vadim Osipov
//	Date: 10/17 


#pragma once

#include "../SDL2/include/SDL.h"
#include "../Components/InteractibleComp.h"

class Structure
{
protected:
	//Storing loop pos
	int m_posX, m_posY;

	//Reference to our base
	InteractibleComp* m_pBaseTile; 

	
public:
	Structure(InteractibleComp* pBaseTile, int posX, int posY);
};