// World.h
// Name: Vadim Osipov
// Date: 10/17/15

#pragma once

#include "../Utility/Singleton.h"
#include "../Game_Field/GameField.h"

class World : public Singleton<World>
{
	GameField* m_pGameField;

	int m_gridHeight;
	int m_gridWidth;

public: 
	//Will create a game field
	void Initialize();
};