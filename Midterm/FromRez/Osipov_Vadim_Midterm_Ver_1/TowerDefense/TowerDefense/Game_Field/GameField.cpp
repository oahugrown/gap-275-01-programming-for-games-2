#include "GameField.h"

#include "../Factory/GameObjectFactory.h"

#include <fstream>
#include <vector>
#include <string>

GameField::GameField(int sizeX, int sizeY)
	: m_sizeX(sizeX)
	, m_sizeY(sizeY)
{
	m_tempCharArray = new char[m_sizeX * m_sizeY];

	m_ppGameField = new Tile*[m_sizeX * m_sizeY];

	for (int i = 0; i < (m_sizeX * m_sizeY); ++i)
	{
		m_tempCharArray[i] = '@';
		m_ppGameField[i] = nullptr;
	}
}

GameField::~GameField()
{
	for (int i = 0; i < (m_sizeX * m_sizeY); ++i)
	{
		delete m_ppGameField[i];
	}
	delete[] m_ppGameField; 
	m_ppGameField = nullptr;

	delete[] m_tempCharArray;
	m_tempCharArray = nullptr;
}

void GameField::Initialize()
{
	LoadMap();
	for (int y = 0; y < m_sizeY; ++y)
	{
		for (int x = 0; x < m_sizeX; ++x)
		{
			int index = (y * m_sizeX) + x;
			GameObjectFactory::GetInstance()->CreateGroundTile(m_tempCharArray[index], this, x, y);

            //Tile* pTile = GameObjectFactory::GetInstance()->CreateGroundTile(m_tempCharArray[index]);
            //SetTile(pTile, index);
		}
	}
}

void GameField::LoadMap()
{
	std::ifstream fin("Level_One.txt");
	if (!fin)
	{
		SDL_Log("FAILED TO LOAD LEVEL");
		return;
	}
	std::vector<std::string> loaded;
	std::string line;

	while (std::getline(fin, line))
		loaded.push_back(line);

	for (int y = 0; y < m_sizeY; ++y)
	{
		for (int x = 0; x < m_sizeX; ++x)
		{
			int index = (y * m_sizeX) + x;
			m_tempCharArray[index] = loaded[y][x];
		}
	}
}
