//	RendererComp.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include "../SDL2_image/include/SDL_image.h"

#include "../Components/Component.h"

class RendererComp : public Component
{
	int m_zOrder;

	SDL_Texture* m_pTexture;
	SDL_Surface* m_pSurface;

public: 
	RendererComp(int zOrder, char* pTextureLocation, int posX, int posY);
	~RendererComp();

	virtual void AddToManager() override;
	
	// Returns ZOrder of this object - never used it yet
	int GetZOrder() { return m_zOrder; }
	//Returns Texture of this object
	SDL_Texture* GetTexture() { return m_pTexture; }

	SDL_Rect* CreateRect(int x, int y);
	

	void ShutDown();

public: 
	bool LoadTexture(char* textureLocation);
};