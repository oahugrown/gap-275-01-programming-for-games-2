//	Player.cpp
//	Name: Vadim Osipov 
//	Date: 10/26

#pragma once

//---------------------------------------------------------------------------------------------------------------------
//	Unique class - Responcible for storing player information
//	will no be controlled by any manager nor it will have any behavior/components on it. 
//---------------------------------------------------------------------------------------------------------------------
class Player
{
	//Health
	int m_healt; 
	//Score - I'm not sure if I'll end up using it
	int m_score;
	//Gold - Used to build towers
	int m_gold;
public:
	Player(int health, int score = 0, int gold = 0);
	~Player() { };

	void Damage(int damage) { m_healt -= damage; }
	
	int GetScore() const { return m_score; }
	void SetScore(int amount) { m_score += amount; }
	
	int GetHealth() const { return m_healt; }
	void SetHealth(int value) { m_healt += value; }
	
	int GetGold() const { return m_gold; }
	void SetGold(int value) { m_gold += value; }
};