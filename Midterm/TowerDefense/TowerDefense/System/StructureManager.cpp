//	StructureManager.cpp
//	Name: Vadim Osipov 
//	Date: 10/24

#include "StructureManager.h"

#include "../Structures/Behaviors/ArcherTowerBeahvior.h"
#include "../Structures/Prejectiles/Projectile.h"

#include "../Base_Classes/GamePiece.h"

#include <assert.h>
#include "../Utility/UtilityFunctions.h"

StructureManager::~StructureManager()
{
	for (ArcherTowerBehavior* pTower : m_archerTowers)
		delete pTower;
	m_archerTowers.clear();

	m_activeProjectiles.clear();

	if (m_myGUID)
		delete m_myGUID;
	m_myGUID = nullptr;

}


void StructureManager::AddStructure(ArcherTowerBehavior* pStructure)
{
	m_archerTowers.push_back(pStructure);
}

void StructureManager::StructuresShoot(SDL_Point* pPoint)
{
	if (m_archerTowers.size() <= 1)
		return;

	for (ArcherTowerBehavior* pStructure : m_archerTowers)
	{
		pStructure->ShootAt(pPoint);
	}
}
void StructureManager::AddProjectile(ProjectileBehavior* pProjectile)
{
	assert(pProjectile);
	m_activeProjectiles[pProjectile->GetGamePiece()->GetGUID().Data1] = pProjectile;
}
void StructureManager::RemoveProjectile(ProjectileBehavior* pProjectile)
{
	m_activeProjectiles.erase(pProjectile->GetGamePiece()->GetGUID().Data1);
}


void StructureManager::Update(Uint32 timePast)
{
	UpdateProjectiles();
}

void StructureManager::UpdateProjectiles()
{
	for (std::map<DWORD, ProjectileBehavior*>::iterator itProjectile = m_activeProjectiles.begin(); itProjectile != m_activeProjectiles.end(); ++itProjectile)
	{
		itProjectile->second->Execute();
	}
	//pop que 
}

void StructureManager::OnInitialize()
{
	//Add yourself to the world 
	World::GetInstance()->AddManager((Manager*)this);
}
void StructureManager::OnShutdown()
{
	//Remove yourself from the world
	World::GetInstance()->RemoveManager((Manager*)this);
}
void StructureManager::DeleteGamePiece(GamePiece* pGamePiece)
{
	//Delete GamePiece you have
}

GUID* StructureManager::GetManagerGUID()
{
	if (m_myGUID == nullptr)
		m_myGUID = &CreateGIUD();

	return m_myGUID;
}

