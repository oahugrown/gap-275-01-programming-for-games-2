//	EnemySpawnBehavior.h
//	Name: Vadim Osipov 
//	Date: 10/20

#pragma once 

#include "TileBehavior.h"


class GamePiece;

class EnemySpawnBehavior : public TileBehavior
{
public: 
	EnemySpawnBehavior(GamePiece* pMyBase);
private: 
	void AddToManger();
};