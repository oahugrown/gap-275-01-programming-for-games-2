//	Projectile.h
//	Name: Vadim Osipov 
//	Date: 10/24

#pragma once

#include "../../SDL2/include/SDL.h"
//Inherits
#include "../../Base_Classes/Behavior.h"
#include "../../Base_Classes/Observer.h"
#include "windows.h"

class ProjectileBehavior : public Behavior, public Observer 
{
	float m_lerpPercent;
	SDL_Point* m_pTarget;
	
	bool m_shouldDestroy;

public: 
	//Observer - pure virtuals
	virtual void OnSpawn() override;
	virtual void OnDestroy() override;
	virtual void Update(Observable* pObservable) override;
	virtual const GUID GetGUID() override;

	bool GetShouldDestroy() { return m_shouldDestroy; }

	ProjectileBehavior(SDL_Point* pTarget, GamePiece* myBase, float lerpPercent);
	~ProjectileBehavior();

	virtual void Execute() override;
};
