#include "../SDL2_image/include/SDL_image.h"

#include "RendererComp.h"
#include "Renderer.h"

RendererComp::RendererComp(int zOrder, char* pTextureLocation, int posX, int posY)
	: Component(posX, posY)
	, m_zOrder(zOrder)
	, m_pSurface(nullptr)
{
	if (!LoadTexture(pTextureLocation))
		SDL_Log("RendererComp(): Couldn't load texture for: %s", pTextureLocation);
	else
	{
		CreateRect(posX, posY);
		AddToManager();

	}
}

RendererComp::~RendererComp()
{
	if (m_pSurface)
	{
		m_pSurface = nullptr;
	}
	if (m_pTexture)
	{
		SDL_DestroyTexture(m_pTexture);
		m_pTexture = nullptr;
	}
}


bool RendererComp::LoadTexture(char* textureLocation)
{
	//Creating surface
	m_pSurface = IMG_LoadPNG_RW(SDL_RWFromFile(textureLocation, "rb"));
	if (!m_pSurface)
	{
		SDL_Log("Couldn't load: %s", textureLocation);
		return false;
	}

	//Creating texture
	m_pTexture = SDL_CreateTextureFromSurface(Renderer::GetInstance()->GetRenderer(), m_pSurface);
	if (!m_pTexture)
	{
		SDL_Log("Failed to create a texture for: %s", textureLocation);
		return false;
	}

	//Sucsess 
	return true;
}


void RendererComp::AddToManager()
{
	Renderer::GetInstance()->AddObject(this);
}

SDL_Rect* RendererComp::CreateRect(int x, int y)
{
	//Creating new SDL_rect for us
	m_pRect = new SDL_Rect;

	//Setting up rect
	m_pRect->x = x * m_pSurface->w;
	m_pRect->y = y * m_pSurface->h;
	m_pRect->h = m_pSurface->h;
	m_pRect->w = m_pSurface->w;

	//Freeing Surface after we are completely done with it
	/// [josh] This is an odd side effect to have from a method called CreateRect.
	/// You can also leak this surface if this is never called and the component is destroyed.
	SDL_FreeSurface(m_pSurface);

	return m_pRect;
}

void RendererComp::ShutDown()
{

	//if (m_pTexture)
	//{
	//	SDL_DestroyTexture(m_pTexture);
	//	m_pTexture = nullptr;
	//}
}