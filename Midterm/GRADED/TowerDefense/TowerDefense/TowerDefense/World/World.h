// World.h
// Name: Vadim Osipov
// Date: 10/17/15

#pragma once

#include "../Utility/Singleton.h"
#include "../Base_Classes/Manager.h"

#include "../Game_Field/GameField.h"

#include <map>
#include "windows.h"

#include "../Player/Player.h"

class World : public Singleton<World>, public Manager 
{

	Player* m_pPlayer;
	GameField* m_pGameField;
	
	int m_gridHeight;
	int m_gridWidth;

	bool m_isGameOver = false;

	Uint32 m_timePast;

	std::map<DWORD, Manager*> m_managers;

public:

	//Worlds specific
	void AddManager(Manager* pManager);
	void RemoveManager(Manager* pManager);

	//Manager pure virtuals
	virtual void Update(Uint32 timePast) override;
	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override { return nullptr; };

	//Player accsessors
	void SetPlayerHelth(int value)	{ m_pPlayer->SetHealth(value); }
	void SetPlayerScore(int value)	{ m_pPlayer->SetScore(value); }
	void SetPlayerGold(int value)	{ m_pPlayer->SetGold(value); }

	bool IsGameOver();

	//Will create a game field
	void Initialize();
	//Get world size
	void GetSize(int& x, int& y) { x = m_gridWidth; y = m_gridHeight; }
	Uint32 GetTimePast() const { return m_timePast; }

private:
	~World();
};