//	EventHandler.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include "../SDL2/include/SDL.h"
#include "../Utility/Singleton.h"

#include "InputManager.h"

//#include "../Components/InteractibleComp.h"
class InteractibleComp;
#include <deque>

//To Rez: Is this class doing to much? It intended to be just an event handler; but now its turning out to be Interactible Manager as well. 
// [rez] No, I don't think so.  It looks like you're delegating the handling of those interactions
// to the InputManager.  I think if any more of that code snuck in here, it would be too much, but 
// you're okay for now.

class EventHandler : public Singleton<EventHandler>
{
	bool m_isAppRunning = true; 
	SDL_Point* m_pMousePoint;

	Uint32 m_startTime; 
	Uint32 m_timePast;

public:
	//void AddObject(InteractibleComp* pObject);
	void HandleEvent(SDL_Event windowEvent);
	//Getter, nothinng more.
	bool GetIsAppRunning() { return m_isAppRunning; }

	//Returns time that has past
	Uint32 TimePast() const { return m_timePast; }

	//Used to initialize Event Handler
	void Initialize();

private:
	~EventHandler() { if (m_pMousePoint) delete m_pMousePoint; m_pMousePoint = nullptr; }
};