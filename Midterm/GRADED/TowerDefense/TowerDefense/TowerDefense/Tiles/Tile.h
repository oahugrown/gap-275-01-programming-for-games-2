//	Tile.h
//	Name: Vadim Osipov
//	Date: 10/16/15

#pragma once

#include "../SDL2/include/SDL.h"

#include "../Components/Component.h"

#include "../Base_Classes/GamePiece.h"

class RendererComp;
class TileBehavior;
//---------------------------------------------------------------------------------------------------------------------
// To Rez: 
//		This class is to be highly refactored.
//		I'm thinking to fully get rid of subclasses and keep it to effects/textures instead of actual subclassing. 
//---------------------------------------------------------------------------------------------------------------------

// [rez] In what way?  Your tiles aren't a base class, they seem to be a subclass of GamePiece, which 
// I don't think is terrible.  You're reusing the concept of behaviors, so this seems fine.

class Tile : public GamePiece
{
	//Saving position of this tile
	SDL_Rect* m_pMyRect;
	//TileBehavior* m_MyBehavior;

public: 
	Tile(RendererComp* pRenderer, GUID myID);
	virtual ~Tile();

	virtual void AddBehavior(Behavior* pBehavior) override;
	virtual void RemoveBehavior(Behavior* pBehavior) override;
	//void Act();
	virtual void Execute() override;


};