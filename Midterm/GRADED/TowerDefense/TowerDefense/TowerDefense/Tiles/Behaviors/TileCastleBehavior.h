//	TileCastleBehavior.h
//	Name: Vadim Osipov 
//	Date: 10/21

#pragma once

#include "../../Base_Classes/Behavior.h"
#include "../../Utility/Direction.h"

#include "../../SDL2/include/SDL.h"

class GamePiece;

class TileCastleBehavior : public Behavior
{
	Direction m_myDirection;

public:
	TileCastleBehavior(GamePiece* pMyBase, int posX, int posY, Direction myDirection);
	~TileCastleBehavior() { }

	//Returning this tile - For enemies to walk on
	virtual Behavior* GetBehavior() { return this; }

	Direction GetTileDirection() const { return m_myDirection; }

	void GetPosition(Uint32& x, Uint32& y);

	void OnEnterCenterOfTile(GamePiece* pPiece);

};