
#include "TowerBaseBehavior.h"

#include "../../Base_Classes/GamePiece.h"

#include "../../Factory/GameObjectFactory.h"

TowerBaseBehavior::TowerBaseBehavior(GamePiece* pMyBase)
	: Behavior(pMyBase)
{

}

void TowerBaseBehavior::Execute()
{
	//Do the building part here
	GameObjectFactory::GetInstance()->CreateStructure(m_pMyBase);
}


