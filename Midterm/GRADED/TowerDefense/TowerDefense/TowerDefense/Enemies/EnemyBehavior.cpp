#include "EnemyBehavior.h"

//Managers 
#include "../System/EnemyManager.h"
#include "../System/ObserverManager.h"


#include "../Tiles/Behaviors/TileRoadBehavior.h"

#include "../World/World.h"

EnemyBehavior::EnemyBehavior(GamePiece* pMyBase, int movementSpeed, int unitHealth)
	: Behavior(pMyBase)
	, m_movementSpeed(movementSpeed)	
	, m_myDirection(Direction::k_west)	//Setting direction to west - kinda cheating, had no time to make it better
	, m_health(unitHealth)
	, m_isDestroyed(false)
{
	Attach();
}

void EnemyBehavior::Execute()
{
	
	UpdateDirection();
	//Tell the GamePiece Base to move this object
	m_pMyBase->MoveMe(m_movementSpeed, m_myDirection);
	//Notify observers on movement
	Notify();
}


void EnemyBehavior::UpdateDirection()
{
	//Getting my position
	Uint32 myPosX, myPosY;
	
	if (!m_pMyBase || !m_pMyBase->GetRect())
		return;

	m_pMyBase->GetRectPosition(myPosX, myPosY);

	for (TileRoadBehavior* pRoadTile : m_roadTiles)
	{
		Uint32 tilePosX, tilePosY;
		pRoadTile->GetPosition(tilePosX, tilePosY);
		if ((myPosX == tilePosX) && (myPosY == tilePosY))
		{
			m_myDirection = pRoadTile->GetTileDirection();
			break;
		}
	}
}

void EnemyBehavior::SetMovingDirection(Direction newDirection)
{
	if (newDirection != Direction::k_none)
		m_myDirection = newDirection;
	else
	{
		World::GetInstance()->SetPlayerHelth(-20);
		Detach();
	}
}

void EnemyBehavior::Attach()
{
	//Attach yourself to Enemy Manager
	EnemyManager::GetInstance()->AddEnemy(this);
	//Attach yourself to Observer Manager
	ObserverManager::GetInstance()->AddObservable((Observable*)this);
}

void EnemyBehavior::Detach()
{
	//To be deleted
	m_pMyBase->DestroyItself();
	
	ObserverManager::GetInstance()->RemoveObservable((Observable*)this);
	EnemyManager::GetInstance()->RemoveEnemy(this);

	m_isDestroyed = true;
	
	//delete this;
}

void EnemyBehavior::Notify()
{
	//should notify observers about its change
	ObserverManager::GetInstance()->GetUpdates((Observable*)this);
}

const GUID EnemyBehavior::GetGUID()
{
	return m_pMyBase->GetGUID();
}


const SDL_Point* EnemyBehavior::GetThisPoint()
{
	SDL_Point* pMyPoint = new SDL_Point;
	pMyPoint->x = m_pMyBase->GetRect()->x;
	pMyPoint->y = m_pMyBase->GetRect()->y;

	return pMyPoint;
}
void EnemyBehavior::ReceiveMessage(int value)
{ 
	m_health += value; 
	if (m_health <= 0)
		Detach();
};

void EnemyBehavior::DestroyThis()
{
	delete this;
}