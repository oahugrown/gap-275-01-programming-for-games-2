//	Object.h
//	Name: Vadim Osipov
//	Date: 11/11

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <vector>
#include <string>

#include "../Globals/Globals.h"

class Object
{
	//Storing 
	std::vector<Face> m_faces;
	std::vector<Vertex> m_vertex;
	
	std::string m_fileName;
public:
	Object(std::string myFileName, std::vector<Face> myFaces, std::vector<Vertex> myVertex);

	const std::vector<Face> GetObjectFaces() const { return m_faces; }
	const std::vector<Vertex> GetObjectVertex() const { return m_vertex; }
};


#endif //__OBJECT_H__