#include <SDL.h>
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>

#include <cml/cml.h>

void CheckGLError()
{
	GLenum error = glGetError();
	if (error != 0)
	{
		SDL_Log("GL Error: %d %s", error, glewGetErrorString(error));
		__debugbreak();
	}
}

GLuint LoadShader(GLenum shaderType, const char* shaderCode)
{
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderCode, nullptr);
	glCompileShader(shader);
	GLint success = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		char shaderLog[1024] = { 0 };
		glGetShaderInfoLog(shader, _ARRAYSIZE(shaderLog), nullptr, shaderLog);
		SDL_Log("Failed to compile shader: %s", shaderLog);
		__debugbreak();
		// Make sure to delete the shader object!
		glDeleteBuffers(1, &shader);
		return 0;
	}

	return shader;
}

void GLAPIENTRY PrintGLDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
	const GLchar* message, const void* userParam)  // must add const to userParam, documentation is incorrect!
{
	SDL_Log("GL Error: %d %s", id, message);
	if (type == GL_DEBUG_TYPE_ERROR)
		__debugbreak();
}

int main(int argc, char* argv[])
{
	SDL_Init(SDL_INIT_VIDEO);

	// Minimally OpenGL 4.0!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	// Tell GL we want a context that supports debugging!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

	SDL_Window* window = SDL_CreateWindow("OpenGL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		800, 600,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	glewExperimental = GL_TRUE;
	glewInit();
	// After this point, the GL function pointers should have been set up by GLEW so they can
	// be called!

	SDL_Log("OpenGL Version: %s", glGetString(GL_VERSION));
	SDL_Log("GLEW Version: %s", glewGetString(GLEW_VERSION));

	glGetError(); // Glew tends to generate GL_INVALID_ENUM (https://www.opengl.org/wiki/OpenGL_Loading_Library)

	glDebugMessageCallback(&PrintGLDebugMessage, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

	const float triangle[] =
	{
		// X    Y    Z
		0.0f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
	};

	GLuint triangleBuffer;
	// Use glGenBuffers if glCreateBuffers crashes (OpenGL 4.5 not available)
	glCreateBuffers(1, &triangleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Fake an error by binding a bogus buffer!
	//glBindBuffer(GL_ARRAY_BUFFER, 84783);

	const int indices[] =
	{
		0, 1, 2
	};
	GLuint triangleIndexBuffer;
	glCreateBuffers(1, &triangleIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	const char* vertexShaderCode =
	{
		"#version 400\n" // This is the only line that requires a newline, all others do not need it!
		"in vec3 vertex;\n"
		"uniform mat4 transformMatrix = mat4(1.0);"
		"void main() {\n"
		"  gl_Position = transformMatrix * vec4(vertex, 1.0);\n"
		"}\n"
	};

	GLuint vertexShader = LoadShader(GL_VERTEX_SHADER, vertexShaderCode);
	if (!vertexShader)
	{
		return 0;
	}

	const char* fragmentShaderCode =
	{
		"#version 400\n" // This is the only line that requires a newline, all others do not need it!
		"out vec4 colorRGBA;"
		"uniform vec4 objectColor = vec4(1.0, 0.0, 0.0, 1.0);"
		"void main() {"
		"  colorRGBA = objectColor;"
		"}"
	};

	GLuint fragmentShader = LoadShader(GL_FRAGMENT_SHADER, fragmentShaderCode);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	GLint linkSuccess = GL_FALSE;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkSuccess);
	if (linkSuccess == GL_FALSE)
	{
		char programLog[1024] = { 0 };
		glGetProgramInfoLog(shaderProgram, _ARRAYSIZE(programLog), nullptr, programLog);
		SDL_Log("Failed to link shaders: %s", programLog);
		__debugbreak();
		glDeleteProgram(shaderProgram);
		return 0;
	}

	GLuint vertArray;
	glGenVertexArrays(1, &vertArray);
	glBindVertexArray(vertArray);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLint objectColorUniform = glGetUniformLocation(shaderProgram, "objectColor");
	if (objectColorUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}

	GLint transformMatrixUniform = glGetUniformLocation(shaderProgram, "transformMatrix");
	if (transformMatrixUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}

	float value = 0.0f;

	cml::matrix44f_c transformMatrix;
	transformMatrix.identity();
	cml::matrix_translation(transformMatrix, 0.1f, 0.5f, 0.0f);

	bool done = false;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				done = true;
				break; 
			}
		}

		glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		if (value > 1.0f)
			value = 0.0f;
		else
			value += 0.1f;

		glUseProgram(shaderProgram);
		glBindVertexArray(vertArray);
		glProgramUniform4f(shaderProgram, objectColorUniform, value, 0.0f, 1.0f, 1.0f);
		glProgramUniformMatrix4fv(shaderProgram, transformMatrixUniform, 1, GL_FALSE, transformMatrix.data());
		//glDrawArrays(GL_TRIANGLES, 0, _ARRAYSIZE(indices));
		glDrawElements(GL_TRIANGLES, _ARRAYSIZE(indices), GL_UNSIGNED_INT, 0);

		SDL_GL_SwapWindow(window);
	}

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}