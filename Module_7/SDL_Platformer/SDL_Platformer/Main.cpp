#include <SDL.h>
#include <SDL_image.h>

#include <stdlib.h>

SDL_Window* g_window = nullptr; 
SDL_Renderer* g_renderer = nullptr;

SDL_Surface* g_bmp = nullptr;
SDL_Texture* g_bmpTexture = nullptr;
SDL_Texture* g_bmpTexture2 = nullptr;

SDL_Surface* gTilesSurface = nullptr;
//SDL_Texture*  gTilesTexture = nullptr;


bool g_isRunning = false;

const int g_windowWidth = 640;
const int g_windowHeight = 480;



int g_tileIndex = 0;
const int k_gTileSetsCollums = 9;
const int k_gTileSetsRows = 2;

//int tileSetWidth = 0;
//int tileSetHeight = 0;
//
//int g_tileWidth = 0; 
//int g_tileHeight = 0;

int g_screenCollums = 0;
int g_screenRows = 0;


SDL_Texture* LoadPNGTexture(const char* pFilename)
{
	SDL_Surface* pSurface = IMG_LoadPNG_RW(SDL_RWFromFile(pFilename, "rb"));
	if (pSurface == nullptr)
	{
		SDL_Log("failed to load: %d", SDL_GetError());
		return nullptr;
	}

	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(g_renderer, pSurface);
	if (pTexture == nullptr)
	{
		SDL_Log("failed to load: %d", SDL_GetError());
		return nullptr;
	}

	SDL_FreeSurface(pSurface);

	return pTexture;
}

struct TileSet
{
	int m_colums;
	int m_rows; 
	int m_tileWidth;
	int m_tileHeight;
	int m_textureWidth;
	int m_textureHight;

	SDL_Texture* m_pTexture;
};

void LoadTileMap(const char* pFilename, TileSet* pTileSet, int tileSetColums, int tileSetRows)
{

	pTileSet->m_pTexture = LoadPNGTexture(pFilename);
	if (pTileSet->m_pTexture == nullptr)
		SDL_Log("failed to load: %s", SDL_GetError());


	SDL_QueryTexture(pTileSet->m_pTexture, nullptr, nullptr, &pTileSet->m_textureWidth, &pTileSet->m_textureHight);

	pTileSet->m_colums = tileSetColums;
	pTileSet->m_rows = tileSetRows;

	pTileSet->m_tileWidth = pTileSet->m_textureWidth / pTileSet->m_colums;
	pTileSet->m_tileHeight = pTileSet->m_textureHight / pTileSet->m_rows;

}

void DrawTite(const TileSet& tileSet, int tileIndex, int screenX, int screenY)
{
	int tileIndexX = tileIndex % tileSet.m_colums;
	int tileIndexY = tileIndex / tileSet.m_colums;

	SDL_Rect tileRect;
	tileRect.x = tileIndexX * tileSet.m_tileWidth;
	tileRect.y = tileIndexY * tileSet.m_tileHeight;
	tileRect.w = tileSet.m_tileWidth;
	tileRect.h = tileSet.m_tileHeight;

	SDL_Rect screenTileRect;
	screenTileRect.x = screenX;
	screenTileRect.y = screenY;
	screenTileRect.w = tileRect.w;
	screenTileRect.h = tileRect.h;

	SDL_RenderCopy(g_renderer, tileSet.m_pTexture, &tileRect, &screenTileRect);
}

int main(int args, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		return -1;
	}
	//Creating the window
	g_window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, g_windowWidth, g_windowHeight, SDL_WINDOW_SHOWN);
	//Creating renderer
	g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
	

	int currentFrame = 0; //colum index 
	int currentAnimation = 1; //row index
	
	g_isRunning = true;

	SDL_Event event; 

	double angle = 0; 

	//unsigned int lastTicks = SDL_GetTicks(); 
	//unsigned int currentTicks = 0; 

	SDL_Rect bmpRect = { 0 };
	Uint64 frequency = SDL_GetPerformanceFrequency(); 
	Uint64 lastCounter = SDL_GetPerformanceCounter();

	float imageX = 0;
	float velocityX = 0; 
	float exelerationX = 0;


	TileSet worldTiles;
	LoadTileMap("default_tiles_0.png", &worldTiles, k_gTileSetsCollums, k_gTileSetsRows);

	g_screenCollums = g_windowWidth / worldTiles.m_tileWidth;
	g_screenRows = g_windowHeight / worldTiles.m_tileHeight;


	int* pTileArray = new int[g_screenCollums * g_screenRows];
	for (int i = 0; i < (g_screenCollums * g_screenRows); ++i)
	{
		pTileArray[i] = 0;
	}



	TileSet charactersSpriteSheets;
	LoadTileMap("BODY_male_Epic_armors_Golden.png", &charactersSpriteSheets, 9, 4);


	//timer 
	const float timeBetweenAnimFrames = 100.0f;
	float accumTimesinceLastFrameChange = 0.0f;


	while (g_isRunning)
	{
		//---------------------------
		// Capping FPS - Runnitg animation at the time rate, not frame rate. 
		//currentTicks = SDL_GetTicks();
		//unsigned int delta = currentTicks - lastTicks;
		//lastTicks = currentTicks;
		//---------------------------
		
		//---------------------------
		//SDL Way to limit time
		Uint64 currentCounter = SDL_GetPerformanceCounter();
		float delta = (currentCounter - lastCounter) / static_cast<float>(frequency) * 1000;
		lastCounter = currentCounter;
		//---------------------------
		


		//Event reciever
		SDL_PumpEvents();

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT)
			{
				if (event.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					g_isRunning = false;
				}
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_q) //'q'
				{
					g_isRunning = false;
				}
				else if (event.key.keysym.sym == SDLK_UP)
				{
					exelerationX = 0.001f;
				}
			}
			else if (event.type == SDL_KEYUP)
			{
				if (event.key.keysym.sym == SDLK_UP)
				{
					exelerationX = 0;
					currentAnimation = 0;
				}
				else if (event.key.keysym.sym == SDLK_DOWN)
				{
					exelerationX = 0;
					currentAnimation = 1;
				}
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_RIGHT)
				{
					g_isRunning = false;
				}
			}
		}

		//Usually you want to clear the screen first - when dealing with ACCELERATED renderer
		//Color
		SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
		//Clearing screen
		SDL_RenderClear(g_renderer);

	
		//DrawTite(g_tileIndex, 124, 34);

		for (int i = 0; i < (g_screenCollums * g_screenRows); ++i)
		{
			int tileScreenX = (i % g_screenCollums) * worldTiles.m_tileWidth;
			int tileScreenY = (i / g_screenCollums) * worldTiles.m_tileHeight;

			DrawTite(worldTiles, pTileArray[i], tileScreenX, tileScreenY);
		}



		//TIMMING ANIMATIONS
		accumTimesinceLastFrameChange += delta;
		if (accumTimesinceLastFrameChange > timeBetweenAnimFrames)
		{
			currentFrame = (currentFrame + 1) % charactersSpriteSheets.m_colums;
			accumTimesinceLastFrameChange -= timeBetweenAnimFrames;
		}
		DrawTite(charactersSpriteSheets, currentFrame + currentAnimation * charactersSpriteSheets.m_colums, 0, 0);
		//-----




		////Josh lecture: 
		////Can be thought of as function of time
		////angle += 0.01f * delta;

		////Texture?
		////Getting the size of the image
		////SDL_Rect bmpRect;
		////bmpRect.x = 10;
		////bmpRect.y = 10;
		////bmpRect.w = g_bmp->w;
		////bmpRect.h = g_bmp->h;

		//velocityX += (delta * exelerationX);
		//imageX += (delta * velocityX);

		//bmpRect.x = imageX;
		//bmpRect.y = 10;
		//bmpRect.w = g_bmp->w;
		//bmpRect.h = g_bmp->h;

		////Getting screen rect - never used it. 
		//SDL_Rect screenRect;
		//SDL_Point imageCenter;
		//imageCenter.x = bmpRect.x + bmpRect.w / 2;
		//imageCenter.y = bmpRect.y + bmpRect.h / 2;

		////Draw the texture - RenderCopy???? WTF?
		////SDL_RenderCopy(g_renderer, g_bmpTexture, NULL, &bmpRect);
		////



		//SDL_RenderCopyEx(g_renderer, g_bmpTexture, NULL, &bmpRect, angle, &imageCenter, SDL_FLIP_NONE);

		////Box - quick learn
		//SDL_Rect box; 
		//box.x = 10;
		//box.y = 10;
		//box.w = g_windowWidth - 20;
		//box.h = g_windowHeight - 20;
		//SDL_SetRenderDrawColor(g_renderer, 0, 255, 0, 255);
		//SDL_RenderDrawRect(g_renderer, &box);
	
		//Drawing from back buffer
		SDL_RenderPresent(g_renderer);
	}

	delete[] pTileArray;


	//Cleaning up
	SDL_DestroyTexture(worldTiles.m_pTexture);
	SDL_DestroyRenderer(g_renderer);
	SDL_DestroyWindow(g_window);
	SDL_Quit();

	return 0;
}