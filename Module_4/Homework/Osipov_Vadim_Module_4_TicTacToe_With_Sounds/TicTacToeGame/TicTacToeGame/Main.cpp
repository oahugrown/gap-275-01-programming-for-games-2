//Main.cpp

#include "TicTacToeGame.h"

//Header of them all - windows 
#include<Windows.h>


bool g_running = true;

//The user-provided entry point for a graphical Windows-based application.
int CALLBACK WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow
	)
{
	//:: looking in global main scope - dont have to do this - it's just more explisit
	::OutputDebugString(L"Hello from WinMain!\n");

	TicTacToeGame& theGame = TicTacToeGame::GetInstance();

	theGame.StartUp();

	while (theGame.IsAppRunning())
	{
		theGame.Update();
	}

	//If return 0 - that means program was complete
	return 0;
}
