#include <iostream>
#include <cmath>

#include "BoardPiece.h"
#include "GameBoard.h"


//Sets the type of the tyle
BoardPiece::TyleType BoardPiece::SetTyle(BoardPiece::TyleType newTyle)
{
	return m_tyleType = newTyle;
}

void BoardPiece::CheckTilesAround(GameBoard& gameBoard)
{

	//If youre closed, with player cursor and without a bomb 
	if (!m_isOpen && !m_beenChecked)
	{
		m_beenChecked = true;

		//Up/down
		for (int y = -1; y < 2; ++y)
		{
			//Dont check yourself
			if (y == 0)
				continue;
			//Safety check 
			if ((m_position[0] + y) < gameBoard.m_boardSizeY && (m_position[0] + y) >= 0)
			//if ((y + 1) < gameBoard.m_boardSizeY && (y + 1) >= 0)
			{
				//Do the up and down
				if (!gameBoard.m_board[m_position[0] + y][m_position[1]].m_isOpen)
				{
					//If you found a number
					if (gameBoard.m_board[m_position[0] + y][m_position[1]].m_tyleType == BoardPiece::EBombsAround)
					{
						//If you have not been checked before
						if (!gameBoard.m_board[m_position[0] + y][m_position[1]].m_beenChecked)
						{
							//Open the number tile
							gameBoard.m_board[m_position[0] + y][m_position[1]].m_isOpen = true;
							gameBoard.m_board[m_position[0] + y][m_position[1]].m_beenChecked = true;
							gameBoard.DrawBoard();
						}
					}
					//Open the tile 
					else
					{
						gameBoard.m_board[m_position[0] + y][m_position[1]].CheckTilesAround(gameBoard);
						gameBoard.m_board[m_position[0] + y][m_position[1]].m_beenChecked = true;
					}
				}
			}
			//Left/right
			for (int x = -1; x < 2; ++x)
			{
				//Safety check
				if ((m_position[1] + x) < gameBoard.m_boardSizeX && (m_position[1] + x) >= 0)
				{
					if (x == 0)
						continue;

					//Do the up and down
					if (!gameBoard.m_board[m_position[0]][m_position[1] + x].m_isOpen)
					{
						//If you found a number
						if (gameBoard.m_board[m_position[0]][m_position[1] + x].m_tyleType == BoardPiece::EBombsAround)
						{
							//If you have not been checked before
							if (!gameBoard.m_board[m_position[0]][m_position[1] + x].m_beenChecked)
							{
								//Open the number tile
								gameBoard.m_board[m_position[0]][m_position[1] + x].m_isOpen = true;
								gameBoard.m_board[m_position[0]][m_position[1] + x].m_beenChecked = true;
								gameBoard.DrawBoard();

								//Check for corners, Diagonal
								//Sefety check, though at this point it should be safe. 
							}
						}
						//Open the tile, check the next tiles
						else
						{
							gameBoard.m_board[m_position[0]][m_position[1] + x].CheckTilesAround(gameBoard);
							gameBoard.m_board[m_position[0]][m_position[1] + x].m_beenChecked = true;
					
						}
					}
					//Diagonal check
					if ((m_position[0] + y) < gameBoard.m_boardSizeY && (m_position[1] + x) < gameBoard.m_boardSizeX && (m_position[0] + y) >= 0 && (m_position[1] + x) >= 0)
					{
						//If you found a number on diagonal check
						if (gameBoard.m_board[m_position[0] + y][m_position[1] + x].m_tyleType == BoardPiece::EBombsAround)
						{
							if (!gameBoard.m_board[m_position[0] + y][m_position[1] + x].m_beenChecked && !gameBoard.m_board[m_position[0] + y][m_position[1] + x].m_isOpen)
							{
								gameBoard.m_board[m_position[0] + y][m_position[1] + x].m_isOpen = true;
								gameBoard.m_board[m_position[0] + y][m_position[1] + x].m_beenChecked = true;
								gameBoard.DrawBoard();
							}
						}
					}

				}
			}
		}
	}
	m_isOpen = true;
}