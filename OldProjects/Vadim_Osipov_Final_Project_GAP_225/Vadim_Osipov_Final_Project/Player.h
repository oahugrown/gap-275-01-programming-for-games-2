#pragma once

#include <iostream>
#include <string>
struct Player
{
	int *m_position = new int[2];
	int m_score; 
	int m_markersLeft; 
	std::string m_playerName; 


	//Default
	Player()
		: m_score(0)
		, m_markersLeft(0)
		, m_playerName("Player")
	{

	}
	//Overloaded
	Player(std::string newName)
		: m_score(0)
		, m_markersLeft(0)
		, m_playerName("Player")
	{
		m_playerName = newName;
	}
	//Demolisher
	~Player()
	{
		if (m_position != nullptr)
			delete[] m_position;
	}

	int& SetPlayerScore(const int& bombsMarked, const int& tilesLeftClosed);
};