#include <SDL.h>
#include <gl/glew.h>	//BEFORE SDL_OpenGL.h
#include <SDL_OpenGL.h>

int main(int args, char* argv[])
{
	SDL_Init(SDL_INIT_VIDEO);

	//Calling to OpenGL 4.0
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	//SDL Window
	SDL_Window* window = SDL_CreateWindow("SDL OpenGL Demo", 
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		800, 600, 
		SDL_WINDOW_SHOWN |SDL_WINDOW_OPENGL);
	//init things 
	SDL_GLContext context = SDL_GL_CreateContext(window);
	glewInit();
	//Triangle
	const float triangle[] = 
	{
		//	X	Y	Z
		0.0f,	0.5f,	0.0f,
		0.5f,	-0.5f,	0.0f,
		-0.5f,	-0.5f,	0.0f,
	};

	//Creating a buffer
	GLuint triangleBuffer;
	glCreateBuffers(1, &triangleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	//Placing data into buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);



	bool done = false;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				done = true;
				break;
			}
		}


		//OpenGL - writing color into back buffer.
		glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
		//To the screen
		glClear(GL_COLOR_BUFFER_BIT);





		//Presenting -
		SDL_GL_SwapWindow(window);


	}

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;

}