//	Name: Osipov Vadim	
//	Renderer.h

#ifndef __RENDERER_H__
#define __RENDERER_H__


#include <SDL.h>
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>

#include <cml/cml.h>


#include "../Globals/Globals.h"
#include "../Camera/Camera.h"

#include "../Object/Object.h"
#include "../Object/Planet/Planet.h"

#include "../Shader/Shader.h"

class Renderer
{
	GameState m_gameState;
	SDL_GLContext m_context;
	SDL_Window* m_pWindow;
	Camera* m_gameCamera;
	Shader* m_pShader;

	//Planets
	Planet* m_pSun;
	Planet* m_pMercury;
	Planet* m_pVenus;
	Planet* m_pEarth;
	Planet* m_pMoon;
	Planet* m_pMars;
	Planet* m_pJupiter;
	Planet* m_pSaturn;
	Planet* m_pUranus;
	Planet* m_pNeptune;

public:
	Renderer();
	~Renderer();

	void Initialize();

	//Temp stuff
	void DoThings();

	void Update() { };

	GameState GetGameState() const { return m_gameState; }

public:
	
	void CreatePlanets();
	GLuint LoadShader(GLenum shaderType, const char* shaderCode);
	Object* CreateObject(Object* object);
};

#endif