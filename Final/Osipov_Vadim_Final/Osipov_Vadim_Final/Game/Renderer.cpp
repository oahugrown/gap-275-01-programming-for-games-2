
#include "Renderer.h"

#include "SDL.h"
#include <gl/glew.h>	//Glew


#include "../Globals/Macros.h"
#include "../Shaders/FragmentShader.h"
#include "../Shaders/VertexShader.h"
#include "../ObjReader/ObjReader.h"
#include "../Globals/Models.h"

Renderer::Renderer()
	: m_gameState(GameState::k_loading)
	, m_pWindow(nullptr)
	, m_gameCamera(nullptr)	
	, m_pShader(nullptr)
	, m_pSun(nullptr)
	, m_pMercury(nullptr)
	, m_pVenus(nullptr)
	, m_pEarth(nullptr)
	, m_pMoon(nullptr)
	, m_pMars(nullptr)
	, m_pJupiter(nullptr)
	, m_pSaturn(nullptr)
	, m_pUranus(nullptr)
	, m_pNeptune(nullptr)
{
//	m_gameCamera = new Camera();
//	m_pShader = new Shader(g_kFragmentShaderCode, g_kVertexShaderCode);
}

Renderer::~Renderer()
{
	//camera goes
	_SAFE_DELETE_(m_gameCamera);
	_SAFE_DELETE_(m_pShader);
	//Planets go
	_SAFE_DELETE_(m_pSun);
	_SAFE_DELETE_(m_pMercury);
	_SAFE_DELETE_(m_pVenus);
	_SAFE_DELETE_(m_pEarth);
	_SAFE_DELETE_(m_pMoon);
	_SAFE_DELETE_(m_pMars);
	_SAFE_DELETE_(m_pJupiter);
	_SAFE_DELETE_(m_pSaturn);
	_SAFE_DELETE_(m_pUranus);
	_SAFE_DELETE_(m_pNeptune);


	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

void Renderer::CreatePlanets()
{

}

void Renderer::Initialize()
{
    //--------------------------------------------------------------------------------------------------------------------
    //	Init()
    //--------------------------------------------------------------------------------------------------------------------
    SDL_Init(SDL_INIT_VIDEO);
    // Minimally OpenGL 4.0!
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    // Tell GL we want a context that supports debugging!
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    m_pWindow = SDL_CreateWindow("OpenGL",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        800, 600,
        SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    m_context = SDL_GL_CreateContext(m_pWindow);

    glewExperimental = GL_TRUE;
    glewInit();
    // After this point, the GL function pointers should have been set up by GLEW so they can
    // be called!

    SDL_Log("OpenGL Version: %s", glGetString(GL_VERSION));
    SDL_Log("GLEW Version: %s", glewGetString(GLEW_VERSION));

    glGetError(); // Glew tends to generate GL_INVALID_ENUM (https://www.opengl.org/wiki/OpenGL_Loading_Library)

    //	glDebugMessageCallback(&PrintGLDebugMessage, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    m_pShader = new Shader(g_kFragmentShaderCode, g_kVertexShaderCode);
    m_gameCamera = new Camera();

    Object* obj = nullptr;
    m_pSun = new Planet("Sun", 0.1f, 0.1f, -5.0f, CreateObject(obj));
    m_pSun->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());


    Object* objEarth = nullptr;
    m_pEarth = new Planet("Earth", 0.1f, 0.1f, -5.0f, CreateObject(objEarth));
    m_pEarth->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());




    SDL_Log("Faces size: %i", m_pSun->GetPlanetBaseObject()->GetObjectFaces().size());
    SDL_Log("Vertex size: %i", m_pSun->GetPlanetBaseObject()->GetObjectVertex().size());

    SDL_Log("Faces size: %i", m_pEarth->GetPlanetBaseObject()->GetObjectFaces().size());
    SDL_Log("Vertex size: %i", m_pEarth->GetPlanetBaseObject()->GetObjectVertex().size());

    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
}

void Renderer::DoThings()
{

	


	

	//???
	float value = 0.0f;

	cml::matrix44f_c viewMatrix;
	viewMatrix.identity();
	cml::matrix44f_c projectionMatrix;
	projectionMatrix.identity();
	cml::matrix_perspective_xfov_RH(projectionMatrix, 90.0f, 800.0f / 600.0f, 0.1f, 1000.0f, cml::z_clip_neg_one);


	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0f);

	//Culling front face



	bool done = false;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				done = true;
				break;
			}


			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_w)
				{
					value += 0.1f;
					//m_pSun->GetPlanetBaseObject()->PlusTranslation(value, 0.0f, 0.0f);
					//m_pEarth->GetPlanetBaseObject()->PlusTranslation(0.0f, value, 0.0f);
					m_gameCamera->MoveForward(0.1f);
				}

				if (event.key.keysym.sym == SDLK_s)
				{
					value -= 0.1f;
					//m_pSun->GetPlanetBaseObject()->PlusTranslation(value, 0.0f, 0.0f);
					//m_pEarth->GetPlanetBaseObject()->PlusTranslation(0.0f, value, 0.0f);
					m_gameCamera->MoveForward(-0.1f);
				}

				if (event.key.keysym.sym == SDLK_SPACE)
				{
					value = 0.0f;
					m_pEarth->GetPlanetBaseObject()->ResetObjectPosition();
					m_pSun->GetPlanetBaseObject()->ResetObjectPosition();
				}

				if (event.key.keysym.sym == SDLK_d)
					m_gameCamera->MoveRight(0.1f);
				if (event.key.keysym.sym == SDLK_a)
					m_gameCamera->MoveRight(-0.1f);


				if (event.key.keysym.sym == SDLK_e)
					m_gameCamera->Pitch(0.1f);
				if (event.key.keysym.sym == SDLK_q)
					m_gameCamera->Pitch(-0.1f);


				//Debug superfeature
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					done = true;
					break;
				}
			}
		}


		//Drawing things out
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		cml::vector3f cameraPosition = m_gameCamera->GetCameraPosition();

		cml::matrix44f_c cameraRotation;
		cameraRotation.identity();
		//Camera rotation
		cml::matrix_rotate_about_local_x(cameraRotation, 0.0f);
		cml::vector4f cameraForward(0.0f, 0.0f, -1.0f, 0.0f);	//Unit Vector? WTF? 
		cml::vector3f cameraDirection = (cameraRotation * cameraForward).subvector(3);
		cml::matrix_rotate_about_local_x(cameraRotation, 180.0f * value);

		viewMatrix = m_gameCamera->GetTransform();

		//CAMERA POSITIONING
		//View Matrix, Eye position, Eye Direction
		cml::matrix_look_at_RH(viewMatrix, cameraPosition,
			cameraPosition + cameraDirection,
			cml::vector3f(0.0f, 1.0f, 0.0f));


		glUseProgram(m_pShader->GetShaderProgram());
		
		GLuint projectionMatrixUniform = glGetUniformLocation(m_pShader->GetShaderProgram(), "projectionMatrix");
		GLuint viewMatrixUniform = glGetUniformLocation(m_pShader->GetShaderProgram(), "viewMatrix");

		glProgramUniformMatrix4fv(m_pShader->GetShaderProgram(), projectionMatrixUniform, 1, GL_FALSE, projectionMatrix.data());
		glProgramUniformMatrix4fv(m_pShader->GetShaderProgram(), viewMatrixUniform, 1, GL_FALSE, viewMatrix.data());

		m_pSun->GetPlanetBaseObject()->Render();
        m_pEarth->GetPlanetBaseObject()->Render();

		SDL_GL_SwapWindow(m_pWindow);
	}

	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

Object* Renderer::CreateObject(Object* object)
{
	ObjReader reader; 
	object = reader.CreateObjectFromFile(g_kSphereModel);

	return object;
}
