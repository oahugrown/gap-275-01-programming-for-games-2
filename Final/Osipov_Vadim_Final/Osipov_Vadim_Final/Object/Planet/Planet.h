

#ifndef __PLANET_H__
#define __PLANET_H__

#include "../Object.h"

class Planet
{
	const char* m_pName;
	Object* m_pMybaseObject;
public:
	Planet(const char* pName, float posX, float posY, float posZ, Object* myBaseObject);
	~Planet();
	Object* GetPlanetBaseObject() const { return m_pMybaseObject; }
};

#endif