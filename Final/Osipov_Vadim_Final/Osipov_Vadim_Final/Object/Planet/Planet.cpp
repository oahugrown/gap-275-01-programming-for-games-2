

#include "Planet.h"
#include "../../Globals/Macros.h"

Planet::Planet(const char* pName, float posX, float posY, float posZ, Object* myBaseObject)
	: m_pMybaseObject(myBaseObject)
	, m_pName(pName)
{
	m_pMybaseObject->SetObjectPosition(posX, posY, posZ);
}
Planet::~Planet()
{
	_SAFE_DELETE_(m_pMybaseObject);
}


