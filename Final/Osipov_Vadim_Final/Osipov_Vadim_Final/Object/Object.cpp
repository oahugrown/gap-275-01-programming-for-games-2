#include <SDL.h>
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>

#include "Object.h"
#include "../Shaders/FragmentShader.h"
#include "../Shaders/VertexShader.h"


//-----------------------------------------------------------------------------------------------------------------
//	W value explained
//		Point - (x, y, z, 1) - should and can apply translation. Point has Scale, Position and Direction
//		Vector - (x, y, z, 0) - should not apply translation. Vector is ONLY direction 
//-----------------------------------------------------------------------------------------------------------------


Object::Object(std::string myFileName, std::vector<Face> myFaces, std::vector<Vertex> myVertex)
	: m_fileName(myFileName)
	, m_faces(myFaces)
	, m_vertex(myVertex)
{

}

void Object::Initialize(GLuint shaderProgram)
{
	//Creating Vertex buffer
	glGenBuffers(1, &m_objectVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_objectVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, GetObjectVertexArraySize(), m_vertex.data(), GL_STATIC_DRAW);

	//Creating Index buffer
	glGenBuffers(1, &m_objectIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_objectIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, GetObjectFaceArraySize(), m_faces.data(), GL_STATIC_DRAW);


	m_shaderProgram = shaderProgram;



	//Vertex Array
	glGenVertexArrays(1, &m_vertexArray);
	glBindVertexArray(m_vertexArray);

    glBindBuffer(GL_ARRAY_BUFFER, m_objectVertexBuffer);	//Binding buffers of Vertex
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_objectIndexBuffer);	//Binding buffers of Index


    GLuint vertexAtribLoc = glGetAttribLocation(m_shaderProgram, "vertex");	//ERROR CHECK!
    if (vertexAtribLoc == -1)
    {
        SDL_Log("Failed to load: vertexAtribLoc ");
    }
    glEnableVertexAttribArray(vertexAtribLoc);


	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	GLint m_transformMatrixUniform = glGetUniformLocation(m_shaderProgram, "transformMatrix");
	if (m_transformMatrixUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}

	glBindVertexArray(m_vertexArray);

	InitializeTransform();
}

void Object::InitializeTransform()
{
	//Transform
	m_objectTransform.identity();

	//Translation
	m_objPosX = 0.1f;
	m_objPosY = 0.5f;
	m_objPosZ = -5.0f;
	m_objectTranslation.identity();
	cml::matrix_translation(m_objectTranslation, m_objPosX, m_objPosY, m_objPosZ); //
	//Rotation
	m_objRotX = 0.0f;
	m_objRotY = 0.0f;
	m_objRotZ = 0.0f;
	m_objectRotation.identity();
	//cml::matrix_rotate_about_local_y(m_objectRotation, 0.0f);
	//Scale
	m_objScaleX = 0.0f;
	m_objScaleY = 0.0f;
	m_objScaleZ = 0.0f;
	m_objectScale.identity();
}


void Object::PlusTranslation(float plusX, float plusY, float plusZ)
{
	m_objPosX += plusX;
	m_objPosY += plusY;
	m_objPosZ += plusZ;

	cml::matrix_translation(m_objectTranslation, m_objPosX, m_objPosY, m_objPosZ);
}
void Object::PlusRotation(float plusX, float plusY, float plusZ)
{
	m_objRotX += plusX;
	m_objRotY += plusY;
	m_objRotZ += plusZ;
}
void Object::PlusScale(float plusX, float plusY, float plusZ)
{
	m_objScaleX += plusX;
	m_objScaleY += plusY;
	m_objScaleZ += plusZ;
}
//Just a helper
void Object::ResetObjectPosition()
{
	//Translation
	m_objPosX = 0.1f;
	m_objPosY = 0.5f;
	m_objPosZ = -15.0f;
	cml::matrix_translation(m_objectTranslation, m_objPosX, m_objPosY, m_objPosZ);
}

void Object::SetObjectPosition(float posX, float posY, float posZ)
{
	m_objPosX = posX;
	m_objPosY = posY;
	m_objPosZ = posZ;
	cml::matrix_translation(m_objectTranslation, m_objPosX, m_objPosY, m_objPosZ);
}

void Object::Render()
{
	glBindVertexArray(m_vertexArray);
	//color and rendering? 
	glProgramUniformMatrix4fv(m_shaderProgram, m_transformMatrixUniform, 1, GL_FALSE, m_objectTransform.data());	//Placing the object
	glDrawElements(GL_TRIANGLES, GetObjectFaceArraySize(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}