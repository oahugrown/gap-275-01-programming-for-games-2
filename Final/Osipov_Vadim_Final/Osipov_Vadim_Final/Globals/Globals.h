//	Name: Vadim Osipov 
//	Globals.h

#ifndef __GLOBALS_H__
#define __GLOBALS_H__

//Faces
struct Face
{
	int m_indX;
	int m_indY;
	int m_indZ;
};
//Vertex
struct Vertex
{
	float m_x;
	float m_y;
	float m_z;
};

enum class GameState
{
	k_loading,
	k_runnig,
	k_over,
};

#endif //__GLOBALS_H__