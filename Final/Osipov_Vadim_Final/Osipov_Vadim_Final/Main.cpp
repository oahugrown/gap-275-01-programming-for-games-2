//	Name: Vadim Osipov 
//	Main.cpp

#include "vld.h"

#include <SDL.h>		//SDL
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>	//OpenGL 
#include <cml/cml.h>	//Math

#include "Game\Renderer.h"
#include "Globals\Globals.h"
#include "Globals\Macros.h"


int main(int args, char* argv[])
{
	Renderer* renderer = new Renderer();
    renderer->Initialize();
    renderer->DoThings();
	
	_SAFE_DELETE_(renderer);
	return 0;
}