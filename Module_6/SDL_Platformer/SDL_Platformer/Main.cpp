#include <SDL.h>
#include <SDL_image.h>


SDL_Window* g_window = nullptr; 
SDL_Renderer* g_renderer = nullptr;

SDL_Surface* g_bmp = nullptr;
SDL_Texture* g_bmpTexture = nullptr;
SDL_Texture* g_bmpTexture2 = nullptr;


bool g_isRunning = false;

const int g_windowWidth = 640;
const int g_windowHeight = 480;

int main(int args, char* argv[])
{ 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		return -1;
	}
	//Creating the window
	g_window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, g_windowWidth, g_windowHeight, SDL_WINDOW_SHOWN);
	//Creating renderer
	g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
	const char* bmp_file = "Images/img-thing_bmp.bmp";
	g_bmp = SDL_LoadBMP(bmp_file);
	if (!g_bmp)
	{
		//"Failed to load some "
		SDL_Log("Failed to load bit map %s: %s", bmp_file, SDL_GetError());
	}

	g_bmpTexture = SDL_CreateTextureFromSurface(g_renderer, g_bmp);
	if (!g_bmpTexture)
	{
		//"Failed to load some "
		SDL_Log("Failed to load bit map %s: %s", g_bmpTexture, SDL_GetError());
	}
	//Delay
	//SDL_Delay(3000);



	g_isRunning = true;

	SDL_Event event; 

	double angle = 0; 

	//unsigned int lastTicks = SDL_GetTicks(); 
	//unsigned int currentTicks = 0; 





	SDL_Rect bmpRect = { 0 };
	Uint64 frequency = SDL_GetPerformanceFrequency(); 
	Uint64 lastCounter = SDL_GetPerformanceCounter();

	float imageX = 0;
	float velocityX = 0; 
	float exelerationX = 0;

	while (g_isRunning)
	{
		//---------------------------
		// Capping FPS - Runnitg animation at the time rate, not frame rate. 
		//currentTicks = SDL_GetTicks();
		//unsigned int delta = currentTicks - lastTicks;
		//lastTicks = currentTicks;
		//---------------------------
		
		//---------------------------
		//SDL Way to limit time
		Uint64 currentCounter = SDL_GetPerformanceCounter();
		float delta = (currentCounter - lastCounter) / static_cast<float>(frequency) * 1000;
		lastCounter = currentCounter;
		//---------------------------
		
		//Event reciever
		SDL_PumpEvents();

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT)
			{
				if (event.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					g_isRunning = false;
				}
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_q) //'q'
				{
					g_isRunning = false;
				}
				else if (event.key.keysym.sym == SDLK_UP)
				{
					exelerationX = 0.001f;
				}
			}
			else if (event.type == SDL_KEYUP)
			{
				if (event.key.keysym.sym == SDLK_UP)
				{
					exelerationX = 0;
				}
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_RIGHT)
				{
					g_isRunning = false;
				}
			}
		}

		//Usually you want to clear the screen first - when dealing with ACCELERATED renderer
		//Color
		SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
		//Clearing screen
		SDL_RenderClear(g_renderer);
		
		//Josh lecture: 
		//Can be thought of as function of time
		//angle += 0.01f * delta;

		//Texture?
		//Getting the size of the image
		//SDL_Rect bmpRect;
		//bmpRect.x = 10;
		//bmpRect.y = 10;
		//bmpRect.w = g_bmp->w;
		//bmpRect.h = g_bmp->h;

		velocityX += (delta * exelerationX);
		imageX += (delta * velocityX);

		bmpRect.x = imageX;
		bmpRect.y = 10;
		bmpRect.w = g_bmp->w;
		bmpRect.h = g_bmp->h;

		//Getting screen rect - never used it. 
		SDL_Rect screenRect;
		SDL_Point imageCenter;
		imageCenter.x = bmpRect.x + bmpRect.w / 2;
		imageCenter.y = bmpRect.y + bmpRect.h / 2;

		//Draw the texture - RenderCopy???? WTF?
		//SDL_RenderCopy(g_renderer, g_bmpTexture, NULL, &bmpRect);
		//



		SDL_RenderCopyEx(g_renderer, g_bmpTexture, NULL, &bmpRect, angle, &imageCenter, SDL_FLIP_NONE);

		//Box - quick learn
		SDL_Rect box; 
		box.x = 10;
		box.y = 10;
		box.w = g_windowWidth - 20;
		box.h = g_windowHeight - 20;
		SDL_SetRenderDrawColor(g_renderer, 0, 255, 0, 255);
		SDL_RenderDrawRect(g_renderer, &box);
		
		
		
		//Drawing from back buffer
		SDL_RenderPresent(g_renderer);





	}

	//Cleaning up
	SDL_DestroyRenderer(g_renderer);
	SDL_DestroyWindow(g_window);
	SDL_Quit();

	return 0;
}