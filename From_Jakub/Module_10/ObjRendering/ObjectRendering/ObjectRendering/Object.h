//Object.h

#ifndef OBJECT_H
#define OBJECT_H
#include <vector>

/// Representation of a 3D vertex.
struct Vertex
{
	float x;
	float y;
	float z;
};

/// Representation of a polygonal face.
/// The indexes in a face represents indexes into the Vertex
/// array for which vertices make up a face.
/// Faces with more than three vertices are supported by the obj
/// format, but we only support loading triangles.
struct Face
{
	int x;
	int y;
	int z;
};

class Object
{

private:
	float* m_vertex;
	int* m_indices;
	//std::vector<Vertex> m_vertexVector; //our list of vertecies
	//std::vector<Face> m_faceVector; //our list of faces
	std::vector<float> m_vertexVector;
	std::vector<int> m_faceVector;

public:
	Object(std::vector<Vertex> vertexList, std::vector<Face> faceList);
	~Object();
	//float* GetVertices(){ return m_vertex; }
	//int* GetIndices(){ return m_indices; }
	std::vector<float> GetVertices(){ return m_vertexVector; }
	std::vector<int> GetIndices(){ return m_faceVector; }
};

#endif