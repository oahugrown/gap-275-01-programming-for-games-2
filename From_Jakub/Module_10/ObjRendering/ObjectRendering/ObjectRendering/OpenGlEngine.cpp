//OpenGlEngine.cpp

#include "OpenGlEngine.h"
#include "ObjLoader.h"

const int OpenGlEngine::indices1[] = { 1, 2, 3 };
const int OpenGlEngine::indices2[] = { 2, 3, 4 };
OpenGlEngine::OpenGlEngine()
	: m_pCurrentObject(nullptr)
	, m_numCurrentObject(0)
	, m_numObjects(3)
	//:m_pObjects(nullptr)
{
	//Init the engine
	//sdl init
	SDL_Init(SDL_INIT_VIDEO);
	//error check
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	//Show the window 
	m_pWindow = SDL_CreateWindow("OpenGL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		800, 600,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

	m_context = SDL_GL_CreateContext(m_pWindow);

	glewExperimental = GL_TRUE;
	glewInit();
	//Load all the models

	ObjLoader objLoader;
	m_pObjects[0] = objLoader.LoadObj("triangle.obj");
	m_pObjects[1] = objLoader.LoadObj("quad.obj");
	m_pObjects[2] = objLoader.LoadObj("hello.obj");
	SwitchModel();
}


OpenGlEngine::~OpenGlEngine()
{
	SDL_Quit();
	SDL_DestroyWindow(m_pWindow);
	SDL_GL_DeleteContext(m_context);
	//delete[] m_pObjects;
}

void OpenGlEngine::CreateBuffer()
{
	unsigned int vertSize = m_pCurrentObject->GetVertices().size()* sizeof(m_pCurrentObject->GetVertices()[0]);
	unsigned int indSize = m_pCurrentObject->GetIndices().size()* sizeof(m_pCurrentObject->GetIndices()[0]);
	//indSize -= sizeof(m_pCurrentObject->GetIndices()[0]);

	GLuint triangleBuffer;
	glCreateBuffers(1, &triangleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertSize, &m_pCurrentObject->GetVertices()[0], GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	
	glCreateBuffers(1, &m_triangleIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_triangleIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indSize, &m_pCurrentObject->GetIndices()[0], GL_STATIC_DRAW);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m_pCurrentObject->GetIndices()), m_pCurrentObject->GetIndices(), GL_STATIC_DRAW);
	 
	
	const char* pVertexShaderCode[] =
	{
		"#version 400\n",   //this is the only thing that requires a new line
		"in vec3 vertex;",
		"void main() {",
		"  gl_Position = vec4(vertex, 1.0);",
		"}",
	};
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, _ARRAYSIZE(pVertexShaderCode), pVertexShaderCode, nullptr);
	glCompileShader(vertexShader);

	const char* pFragmentShaderCode[] =
	{
		"#version 400\n",   //this is the only thing that requires a new line
		"out vec4 colorRGBA;",
		"void main() {",
		//" colorRGBA = gl_FragCoord;"

		"  colorRGBA = vec4(1.0, 0.5, 0.0, 0.5);",
		"}",
	};

	const char* pFragmentShaderCode1[] =
	{
		"#version 400\n",   //this is the only thing that requires a new line
		"out vec4 colorRGBA;",
		"void main() {",
		//" colorRGBA = gl_FragCoord;"

		"  colorRGBA = vec4(1.0, 1, 0.0, 0.5);",
		"}",
	};

	const char* pFragmentShaderCode2[] =
	{
		"#version 400\n",   //this is the only thing that requires a new line
		"out vec4 colorRGBA;",
		"void main() {",
		//" colorRGBA = gl_FragCoord;"

		"  colorRGBA = vec4(0.0, 0.5, 0.1, 1.0);",
		"}",
	};
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (m_index == 1)
	{
		m_index = 2;
		glShaderSource(fragmentShader, _ARRAYSIZE(pFragmentShaderCode), pFragmentShaderCode, nullptr);
	}
	else if (m_index == 2)
	{
		m_index = 3;
		glShaderSource(fragmentShader, _ARRAYSIZE(pFragmentShaderCode1), pFragmentShaderCode1, nullptr);
	}
	else
	{
		m_index = 1;
		glShaderSource(fragmentShader, _ARRAYSIZE(pFragmentShaderCode2), pFragmentShaderCode2, nullptr);
	}

	glCompileShader(fragmentShader);


	m_shaderProgram = glCreateProgram();
	glAttachShader(m_shaderProgram, vertexShader);
	glAttachShader(m_shaderProgram, fragmentShader);
	glLinkProgram(m_shaderProgram);
	//---------------------------------------------------------------------------------------

	
	glGenVertexArrays(1, &m_vertArray);
	glBindVertexArray(m_vertArray);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_triangleIndexBuffer);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

bool OpenGlEngine::Update()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
		{
			return true;
		}
		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_SPACE:
				SwitchModel();
				break;
			}
		}
	}
	//const int* indices = &indices2[0];
	//Making the background color
	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	//clearing the old buffer before using it
	glClear(GL_COLOR_BUFFER_BIT);

	//DRAWING PIXELS!----------------------------------------------------
	glUseProgram(m_shaderProgram);
	glBindVertexArray(m_vertArray);
	//draw dem arrays
	//glDrawArrays(GL_TRIANGLES, 0, _ARRAYSIZE(m_pCurrentObject->GetIndices()));
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_pCurrentObject->GetIndices().size() * sizeof(int), &m_pCurrentObject->GetIndices()[0], GL_STATIC_DRAW);
	//glDrawArrays(GL_TRIANGLES, 0, m_pCurrentObject->GetIndices().size() );
	//glDrawElements(GL_TRIANGLES, _ARRAYSIZE(m_pCurrentObject->GetIndices()), GL_UNSIGNED_INT, 0);
	glDrawElements(GL_TRIANGLES, m_pCurrentObject->GetIndices().size() , GL_UNSIGNED_INT, 0);


	//swapping the window buffer
	SDL_GL_SwapWindow(m_pWindow);
	return false;
}

void OpenGlEngine::SwitchModel()
{
	//see if we have assigned anything to the current model
	if (m_pCurrentObject == nullptr)
	{
		m_pCurrentObject = m_pObjects[0];
		m_numCurrentObject = 0;
	}
	else
	{
		m_numCurrentObject += 1;
		if (m_numCurrentObject >= m_numObjects)
		{
			m_numCurrentObject = 0;
		}
		m_pCurrentObject = m_pObjects[m_numCurrentObject];
	}
	CreateBuffer();
}